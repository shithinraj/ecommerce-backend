package com.ecommerce.main.service;

import com.ecommerce.main.dao.ProductDao;
import com.ecommerce.main.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    private ProductDao productDao;
    public Product addNewProduct(Product product){
        return productDao.save(product);
    }
}
